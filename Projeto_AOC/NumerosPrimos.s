# CODIGO ABAIXO - EXECUTADO EM PCSPIM OU MARS
# QUESTÃO: LÊR UM NUMERO ENTRE 1 E 100.000 E RETORNAR
# TODOS OS NUMEROS PRIMOS NO INTERVALO.

.data

	msg0:.asciiz"\n-----Descubra os numeros Primos-----"
	msg1:.asciiz"\nDigite um numero entre 1 e 100000: "
	msg2:.asciiz"\nVoce digitou um numero invalido"
	msg3:.asciiz"\nEstes numeros sao primos:\n"
	espaco:.asciiz"\n"

.text
.globl main
	main:
		# ------------- DEFINIÇÃO DAS ROTINAS DE EXECUÇÃO ----------------------
		jal ler					# Salto para a leitura inicial do programa

		li $t0, 2				# dividir por 2 ate o valor digitado
		li $t7, 0				# comparar com 0
		li $s0, 1				# atribui o ponto de inicio de contagem

		jal verifica		# Salto para a verficação dos valores primos

		j fim						# finaliza o programa

		voltar:					# volta para o endereço passado por ra

		jr $ra					# pula para ra

		# ----------- CORPO PRINCIPAL DE EXECUÇÃ0 - INICIO -------------------
		ler:
		# ---------- CABECALHO DO PROGRAMA - APRESENTACAO --------------------

			# escreval("-----Descuba os numeros Primos-----")
			li $v0, 4								# $v0 recebe 4
			la $a0, msg0						# $a0 recebe mensagem
			syscall									# Chamada de sistema

			# --------- LE O VALOR INFORMADO PELO USUARIO -------------------
			inicio:
				# escreval("Digite um numero entre 1 e 100000")
				li $v0, 4							# $v0 recebe 4
				la $a0, msg1					# $a0 recebe mensagem
				syscall								# Chamada de sistema

				# $t0 - sera usado para armazenar o valor inicial
				# Aqui ficara o valor informado pelo usuario
				li $v0, 5							# $v0 recebe 5
				syscall								# chamada de sistema
				add $s1, $v0, $zero		# $s1 recebe $v0 + 0 sendo $s1 = valor digitado


			# ----------- TESTE VERIFICA VALOR DIGITADO --------------------

			# Se (numero >= 100001) entao
			# Enquanto o valor for maior que 100000 ira ser reiniciado
			bge $s1, 100001, semaior		# $s1 >= 100001 segue para label semaior
			j testemenor								# Caso contrario pula para label testemenor

			semaior:
			  li $v0, 4									# $v0 recebe 4
				la $a0, msg2							# $a0 recebe mensagem
				syscall										# Chamada de sistema
			j inicio										# pula para label inicio

			# Se (numero <= 1) entao
			# Enquanto o valor for menor que 1 ira ser reiniciado
			testemenor:
				ble $s1, 1, semenor				# $s1 <= 1 segue para label semenor
				j segueapp								# Caso contrario pula para label segueapp

			semenor:
				li $v0, 4									# $v0 recebe 4
				la $a0, msg2							# $a0 recebe mensagem
				syscall										# Chamada de sistema
			j inicio										# pula para label inicio

			# ----------- FIM DE TESTE VERIFICA VALOR -------------------------------

			segueapp:

				li $v0, 4									# $v0 recebe 4
				la $a0, msg3							# $a0 recebe mensagem
				syscall										# Chamada de sistema

		# --------------------- FINALIZA O CORPO PRINCIPAL -------------------------

		j voltar											# pula para o voltar

		# ------------- PERCORRE OS VALORES VERIFICANDO OS PRIMOS ------------------
		percorre:

			bge $s0, $s1, voltar				# $s0 >= $s1 segue para label voltar
			addi $s0, $s0, 1						# adiciona + 1 ao valor de $s0

			li $t0, 2										# atribui 2 ao valor de $t0
			j verifica									# pula para o modo de verificacao

		# ------------ FAZ OS CALCULOS PARA ENCONTRAR OS PRIMOS --------------------
		verifica:

			div $s0, $t0								# realiza a divisão dos valores
			mfhi $t2										# move para o registrador $t2

			beq $t2, $t7, percorre			# verifica se $t2 e maior ou igual a $t7
																	# se for, retorna ao label percorre
			addi $t0, $t0, 1						# adiciona + 1 ao valor de $t0

			bge $t0, $s0, primo					# verifica se o valor de $t0 é maior que $s0
																	# se for, pula para o label primo

			j verifica									# realiza o jump de acesso em loop

		# --------- IMPRIME OS VALORES ENCONTRADOS PRIMOS -------------------------
		primo:

			la $a0, espaco							# atribui a mensagem espaco
			li $v0, 4										# imprime um salto de linha
			syscall											# chamada de sistema

			add $a0, $s0, $zero					# atribui o valor para impressão
			li $v0, 1										# imprime o valor primo
			syscall											# chamada de sistema

		j percorre										# Volta para verificação de valores

		# -------------- ENCERRAMENTO DO SISTEMA GERAL ---------------------------
		fim:
			li $v0, 10									# finaliza o sistema
			syscall											# chamada de sistema

# ---------------------------------------------------------------------------
# CÓDIGO CRIADO POR ERISVALDO CORREIA
# PARA MATÉRIA DE ARQUITETURA DE COMPUTADORES
# 2017 - FACULDADE DE TECNOLOGIA
