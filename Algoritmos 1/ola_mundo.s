# ASSEMBLY MIPS
# Programa: Olá Mundo
# Simples impressão de mensagem no terminal

.data
  msg1:.asciiz "\nHELLO WORLD\n"
  msg2:.asciiz "\nLivre da Maldicao em ASSEMBLY"

.text
  main:
    # escreval("Hello World")
    li $v0,4    # $v0 recebe 4
    la $a0,msg1 # $a0 recebe mensagem 1
    syscall     # Chamada de sistema

    # escreval("Livre da maldicao em Assembly")
    li $v0,4    # $v0 recebe 4
    la $a0,msg2 # $a0 recebe mensagem 1
    syscall     # Chamada de sistema
