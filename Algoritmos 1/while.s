# ASSEMBLY MIPS
# Programa: Somar enquanto nao for 0
# Simples exemplo de repetições estilo while

.data
  msg1: .asciiz "\nDigite um valor inteiro: "
  msg2: .asciiz "\nA soma dos valores digitados é "

.text
main:
  li $t0, 0   # iniciando a posição da mémoria

  # estrutura de laço
  enquanto:
    li $v0, 4
    la $a0, msg1
    syscall

    li $v0, 5
    syscall
    add $t1, $v0, $zero
    add $t0, $t0, $t1
    beq $t1, 0, fimenquanto
    j enquanto

  # escrevendo a Soma
  fimenquanto:
    add $s0, $t0, $zero
    li $v0, 4
    la $a0, msg2
    syscall

    li $v0, 1
    add $a0, zero, $s0
    syscall

  # Encerrando
  exit:
    li $v0, 5
    syscall

# Enquanto o valor de entrada for diferente de 0, então
# o programa irá coletar novos valores e ir somando o atual
# ao resultado da soma dos anteriores.
# ao final, exibe o valor da soma e espera um clique para finalizar!
