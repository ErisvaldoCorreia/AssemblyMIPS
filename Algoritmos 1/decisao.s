# ASSEMBLY MIPS
# Programa: Decisões (uso do Jump)
# Verificar se valor é maior que outro

.data
	msg1:.asciiz"\nDigite um numero: "
	msg2:.asciiz"\nMaior: "

.text
  main:
  	# escreval("Digite um numero")
  	li $v0, 4            # $v0 recebe 4
  	la $a0,msg1          # $a0 recebe mensagem
  	syscall              # Chamada de sistema

  	# leia(a), a = $t0
  	li $v0, 5            # $v0 recebe 5
  	syscall              # chamada de sistema
  	add $t0,$v0,$zero    # $t0 recebe $v0 + 0 - sendo $t0 = a

  	# escreval("Digite um numero")
  	li $v0,4             # $v0 recebe 4
  	la $a0,msg1          # $a0 recebe mensagem
  	syscall              # Chamada de sistema

  	# leia(b), b = $t1
  	li $v0,5             # $v0 recebe 5
  	syscall              # chamada de sistema
  	add $t1,$v0,$zero    # $t1 recebe $v0 + 0 - sendo $t1 = b

  	# escreva("maior: ")
  	li $v0,4             # $v0 recebe 4
  	la $a0,msg2          # $a0 recebe mensagem
  	syscall              # Chamada de sistema

  	# Se (a >= b) entao
  	bge $t0,$t1,se       # $t0 >= $t1 segue para label se
  	j senao              # Caso contrario pula para label senao

  	se:
  	  li $v0,1           # $v0 recebe 1
  	  add $a0,$t0,$zero  # $a0 recebe $t0 + 0
  	  syscall            # chamada de sistema
  	j fimse              # pula para label fimse

  	senao:
  	  li $v0,1           # $v0 recebe 1
  	  add $a0,$t1,$zero  # $a0 recebe $t1 + 0
  	  syscall            # chamada de sistema

  	fimse:
