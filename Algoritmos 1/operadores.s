# ASSEMBLY MIPS
# Programa: Operações Matemáticas
# Simples impressão das operações aritméticas

.data
	textosoma:.asciiz	"\nA soma resulta em: "
	textosub:.asciiz	"\nA subtracao resulta em: "
	textomult:.asciiz	"\nA multiplicacao resulta em: "
	textodiv:.asciiz	"\nA divisao, inteiros, resulta em: "
	textoresto:.asciiz	"\nO resto da divisao, resulta em: "
.text
main:
	li $t1, 5 # Valor 5 no registrador t1
	li $t2, 2 # Valor 2 no registrador t2

	add $t3, $t1, $t2	# Regitrador t3 recebe a soma de t1 e t2
	sub $t4, $t1, $t2	# Regitrador t4 recebe a subtração de t1 e t2
	mul $t5, $t1, $t2	# Regitrador t5 recebe a multiplicação de t1 e t2
	div $t6, $t1, $t2	# Regitrador t6 recebe a divisão de t2 e t1
	rem $t7, $t1, $t2	# Regitrador t7 recebe o resto da divisão de t2 e t1

	# Os blocos abaixo, simplesmente exibem os resultados
	# Recebem os textos previos das variaveis
	li $v0, 4
	la $a0, textosoma
	syscall
	li $v0, 1
	add $a0, $t3, $zero
	syscall

	li $v0, 4
	la $a0, textosub
	syscall
	li $v0, 1
	add $a0, $t4, $zero
	syscall

	li $v0, 4
	la $a0, textomult
	syscall
	li $v0, 1
	add $a0, $t5, $zero
	syscall

	li $v0, 4
	la $a0, textodiv
	syscall
	li $v0, 1
	add $a0, $t6, $zero
	syscall

	li $v0, 4
	la $a0, textoresto
	syscall
	li $v0, 1
	add $a0, $t7, $zero
	syscall
